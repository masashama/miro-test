import {Tag} from "./Tag";

/**
 * Описание колбэка для change события
 */
export type ChangeCallback = (tags: Array<Tag>) => any