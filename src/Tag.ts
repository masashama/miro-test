interface Options {
    element?: string,
    closeable?: boolean,
    validateRegexp?: RegExp,
}

/**
 * Просто вынес что бы иметь абстрацию в виде тегов
 * через конфиги можно разные валидаторы накидывать
 * но для теста эт не надо
 * чот оверхеднул
 */
export class Tag {
    private readonly element;
    private readonly validateRegexp: RegExp =  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    constructor(tag: string, options: Options = <Options>{}) {
        if (options.element) this.element = document.createElement(options.element);
        else this.element = document.createElement('span');

        this.element.className = 'tag email-input__tag';
        this.element.textContent = tag.trim();

        const closeButtonElement = document.createElement('i');
        closeButtonElement.className = 'tag__close';
        closeButtonElement.addEventListener('click', this.remove.bind(this));
        this.element.appendChild(closeButtonElement);

        if (options.validateRegexp)
            this.validateRegexp = options.validateRegexp;
    }

    set text(tag: string) {
        this.element.textContent = tag;
    }

    get text(): string {
        return this.element.textContent
    }

    get isValid() {
        return this.validateRegexp.test(this.element.textContent)
    }

    get domElement(): Element {
        return this.element
    }

    private remove() {
        this.element.remove();
        this.element.dispatchEvent(
            new CustomEvent('remove-tag', { detail: this })
        );
    }

    private setInvalid() {
        if (!this.element.classList.contains('tag_invalid'))
            this.element.classList.add('tag_invalid');
    }

    private setValid() {
        if (this.element.classList.contains('tag_invalid'))
            this.element.classList.remove('tag_invalid');
    }

    public validate() {
        if (!this.isValid) this.setInvalid();
        else this.setValid();
    }

}