import '../src/assets/main.styl';
import { Tag } from './Tag';
import { ChangeCallback } from './types'

/**
 * Апофеоз взаимодействия js и dom
 */
class EmailsInput {

    readonly APPEND_TAG_EVENT: string = 'append-tag';

    /**
     * Хотел пойти через абстракцию в виде массива
     * но это оверхед для такого простого поля
     * где не нужны сортировки, будем на прямую в дом пихать.
     * Потому что синхронить массив с домом (ака виртуальный дом на минималках)
     * очень накладно, особенно в части удаления элементов
     */
    private readonly tagContainer: HTMLElement = null;
    private inputField: HTMLInputElement = null;


    constructor(element: Element) {
        this.tagContainer = document.createElement('div');
        this.tagContainer.className = 'email-input';

        this.initInputElement();

        element.replaceWith(this.tagContainer);
    }

    /**
     * Input element for tags
     */
    private initInputElement(): void {
        this.inputField = document.createElement('input');
        this.inputField.type = 'text';
        this.inputField.className = 'email-input__input';
        this.inputField.placeholder = 'add more people...';
        this.inputField.addEventListener('keyup', this.inputEnterListener.bind(this));
        this.inputField.addEventListener('blur', this.inputBlurListener.bind(this));
        this.inputField.addEventListener('paste', this.inputPasteListener.bind(this));

        this.tagContainer.appendChild(this.inputField);
        this.tagContainer.addEventListener('click', () => this.inputField.focus());

    }

    /**
     * onChange listener setter
     * Потенциальная проблема в том что можно накидывать кучу листенеров так
     * а не переприсваивать. Сделал глупо.
     * @param callback
     */
    public set onChange(callback: ChangeCallback) {
        this.tagContainer.addEventListener(this.APPEND_TAG_EVENT, () => {
            callback(this.getTags());
        })
    }


    /**
     * On Enter keypress event listener
     * @param event
     */
    private inputEnterListener(event: KeyboardEvent): void {
        const inputElement = <HTMLInputElement>event.target;
        if (event.code === 'Enter' && inputElement.value.trim()) {
            this.addEmail(inputElement.value.trim());
        }
    }

    /**
     * Lost focus event listener
     * @param event
     */
    private inputBlurListener(event: MouseEvent): void {
        const inputElement = <HTMLInputElement>event.target;
        if (inputElement.value.trim()) {
            this.addEmail(inputElement.value.trim());
        }
    }

    /**
     * Paste event listener
     * @param event
     */
    private inputPasteListener(event: ClipboardEvent): void {
        const data: string = event.clipboardData.getData('text');
        this.addEmail(data.trim());
        event.preventDefault();
    }

    /**
     * Из за того что отказался от хранения тегов в массиве
     * сделал такой ход конем, снова дом в теги загоняю
     * для каунта и получение емейлов
     */
    private getTags(): Array<Tag> {
        const tags: Array<Tag> = [];
        const tagsItems = this.tagContainer.querySelectorAll('.email-input__tag');
        for (let i = 0; i < tagsItems.length; i++) {
            tags.push(new Tag(tagsItems[i].textContent))
        }

        return tags;
    }

    /**
     * Обратный ход конем и тегов в дом рендерем
     * и еще евент кидаем, что тег добавился
     * @param emails
     */
    public addEmail(emails: string): void {
        const emailsList = emails.split(',');

        for(let i = 0; i < emailsList.length; i++) {
            const tag = new Tag(emailsList[i]);
            tag.validate();
            tag.domElement.addEventListener('remove-tag', () => {

                /** времени к сожалению нет что бы сделать нормально просто прокину евент контейнера */
                this.tagContainer.dispatchEvent(
                    new CustomEvent(this.APPEND_TAG_EVENT, { detail: emailsList })
                );
            });
            this.tagContainer.insertBefore(tag.domElement, this.inputField);
        }

        /** кидаем евент о том что список изменился */
        this.tagContainer.dispatchEvent(
            new CustomEvent(this.APPEND_TAG_EVENT, { detail: emailsList })
        );

        this.inputField.value = null;
    }

    /**
     * Count valid emails
     */
    public count(): number {
        return this.getTags().filter((tag: Tag) => tag.isValid).length;
    }


    /**
     * Get all emails
     */
    public getEmails(): Array<string> {
        return this.getTags().map((tag: Tag) => tag.text)
    }

}

/** Examples **/
const inputContainerNode = document.querySelector('#emails-input');
const emailsInput = new EmailsInput(inputContainerNode);

/** Set event listener */
emailsInput.onChange = (tags) => console.log(tags);

/** Count all valid email */
document.querySelector('.count-email').addEventListener('click', () =>
    alert(emailsInput.count())
);

/** random email */
document.querySelector('.add-email').addEventListener('click', () => {
        const domains = ['google.com', 'yandex.ru', 'yahoo.com', 'msdn.net', 'protonmail.com'];
        const names = ['aleksandr', 'vladimir', 'gnu', 'unix', 'lol', 'flex', 'bite', 'zoomer'];
        const random = (max: number) => Math.floor(Math.random() * Math.floor(max));
        emailsInput.addEmail( names[random(8)] + '@' + domains[random(5)]);
    }
);





