/**
 * Полифилы использовать нельзя,
 * но кто сказал что я не могу написать парочку сам xD
 */


/**
 * Add replaceWith for element. IE11
 * @param item
 */
Element.prototype.replaceWith = function (item) {
    if (this.parentNode) {
        this.parentNode.replaceChild(item, this);
    }
};

/**
 * Add remove for element. IE11
 */
Element.prototype.remove = function remove() {
    if (this.parentNode) {
        this.parentNode.removeChild(this);
    }
};

/**
 * Add custom events fo IE11
 */
if ( typeof window.CustomEvent !== 'function' ) {
    // @ts-ignore
    window.CustomEvent = function (event, payload) {
        payload = payload || { bubbles: false, cancelable: false, detail: null };
        const ev = document.createEvent( 'CustomEvent' );
        ev.initCustomEvent( event, payload.bubbles, payload.cancelable, payload.detail );
        return ev;
    }
}